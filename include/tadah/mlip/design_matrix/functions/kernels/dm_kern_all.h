#include <tadah/mlip/design_matrix/functions/kernels/dm_kern_base.h>
#include <tadah/mlip/design_matrix/functions/kernels/dm_kern_linear.h>
#include <tadah/mlip/design_matrix/functions/kernels/dm_kern_rbf.h>
#include <tadah/mlip/design_matrix/functions/kernels/dm_kern_lq.h>
#include <tadah/mlip/design_matrix/functions/kernels/dm_kern_polynomial.h>
#include <tadah/mlip/design_matrix/functions/kernels/dm_kern_sigmoid.h>
#include <tadah/mlip/design_matrix/functions/kernels/dm_kern_quadratic.h>
